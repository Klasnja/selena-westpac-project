package com.example.selenaklasnja.myapplication.util

import com.example.selenaklasnja.myapplication.Schedulers
import io.reactivex.Scheduler

class TestSchedulers : Schedulers() {

    override fun ui(): Scheduler {
        return io.reactivex.schedulers.Schedulers.trampoline()
    }

    override fun io(): Scheduler {
        return io.reactivex.schedulers.Schedulers.trampoline()
    }

    override fun single(): Scheduler {
        return io.reactivex.schedulers.Schedulers.trampoline()
    }

    override fun computation(): Scheduler {
        return io.reactivex.schedulers.Schedulers.trampoline()
    }

    override fun trampoline(): Scheduler {
        return io.reactivex.schedulers.Schedulers.trampoline()
    }
}