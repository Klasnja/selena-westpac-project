@file:Suppress("IllegalIdentifier")

package com.example.selenaklasnja.myapplication.presentation.currencyselection

import com.example.selenaklasnja.myapplication.R
import com.example.selenaklasnja.myapplication.data.Currency
import com.example.selenaklasnja.myapplication.data.managers.NoNetworkException
import com.example.selenaklasnja.myapplication.domain.data.MethodSelection
import com.example.selenaklasnja.myapplication.domain.repositories.AppDataRepository
import com.example.selenaklasnja.myapplication.domain.repositories.FxFeedRepository
import com.example.selenaklasnja.myapplication.presentation.currencyselection.CurrencyListAdapter.CurrencyListViewType.*
import com.example.selenaklasnja.myapplication.presentation.currencyselection.CurrencyListAdapter.CurrencyViewItem
import com.example.selenaklasnja.myapplication.util.TestSchedulers
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Notification
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

/**
 * Created by selenaklasnja on 3/6/18.
 */
class CurrencyListPresenterTest {

    @Mock
    lateinit var view: CurrencyListPresenter.ViewSurface

    @Mock
    lateinit var appDataRepository: AppDataRepository
    @Mock
    lateinit var fxFeedRepository: FxFeedRepository

    lateinit var presenter: CurrencyListPresenter

    //region defaultData
    private val currencyUsd = Currency("USD", "Dollar", "USA")
    private val currencyEur = Currency("EUR", "Euro", "Europe")
    private val currencies: Collection<Currency> = listOf(currencyUsd, currencyEur)
    val usdBuyRate = 0.7
    val usdSellRate = 0.8
    //endregion

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        Mockito.`when`(fxFeedRepository.getCurrencyList())
                .thenReturn(Observable.just(Notification.createOnNext(currencies)))
        Mockito.`when`(fxFeedRepository.getClientBuyExchangeRate("USD"))
                .thenReturn(Observable.just(usdBuyRate))
        Mockito.`when`(fxFeedRepository.getClientSellExchangeRate("USD"))
                .thenReturn(Observable.just(usdSellRate))

        presenter = CurrencyListPresenter(fxFeedRepository, appDataRepository, ListItemsCreator())
        presenter.schedulers = TestSchedulers()
        presenter.onCreate(view)
    }

    @Test
    fun `getCurrencyList - currency list is retreived successful - view is updated correctly`() {
        // when
        presenter.getCurrencyList()

        // then
        verify(view, times(1)).updateList(
                listOf(
                        CurrencyViewItem(PROGRESS_BAR)))
        verify(view, times(1)).updateList(
                listOf(
                        CurrencyViewItem(ITEM, currencyUsd),
                        CurrencyViewItem(ITEM, currencyEur),
                        CurrencyViewItem(FOOTER)))
    }

    @Test
    fun `getCurrencyList - currency list failed - view is updated correctly`() {
        // given
        val throwable = RuntimeException()
        Mockito.`when`(fxFeedRepository.getCurrencyList())
                .thenReturn(Observable.just(Notification.createOnError(throwable)))

        // when
        presenter.getCurrencyList()

        // then
        verify(view, times(1)).updateList(
                emptyList<CurrencyListAdapter.CurrencyViewItem>())
    }

    @Test
    fun `getCurrencyList - currency list failed - user is notified`() {
        // given
        val throwable = RuntimeException()
        Mockito.`when`(fxFeedRepository.getCurrencyList())
                .thenReturn(Observable.just(Notification.createOnError(throwable)))

        // when
        presenter.getCurrencyList()

        // then
        verify(view, times(1)).showError(R.string.error_server)
    }

    @Test
    fun `getCurrencyList - no network exception - user is notified`() {
        // given
        val throwable = NoNetworkException()
        Mockito.`when`(fxFeedRepository.getCurrencyList())
                .thenReturn(Observable.just(Notification.createOnError(throwable)))

        // when
        presenter.getCurrencyList()

        // then
        verify(view, times(1)).showError(R.string.error_network_connection)
    }

    @Test
    fun `onCurrencySelected - buy currency is selected - calculate activity should start`() {
        // given
        Mockito.`when`(appDataRepository.getMethodSelection())
                .thenReturn(MethodSelection.BUY)
        // when
        presenter.onCurrencySelected(currencyUsd)

        // then
        verify(view, times(1))
                .startCalculateActivity(MethodSelection.BUY.ordinal, currencyUsd, usdBuyRate)
    }


    @Test
    fun `onCurrencySelected - sell currency is selected - calculate activity should start`() {
        // given
        Mockito.`when`(appDataRepository.getMethodSelection())
                .thenReturn(MethodSelection.SELL)
        // when
        presenter.onCurrencySelected(currencyUsd)

        // then
        verify(view, times(1))
                .startCalculateActivity(MethodSelection.SELL.ordinal, currencyUsd, usdSellRate)
    }
}