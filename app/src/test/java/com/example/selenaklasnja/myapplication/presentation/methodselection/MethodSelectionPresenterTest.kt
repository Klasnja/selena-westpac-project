@file:Suppress("IllegalIdentifier")
package com.example.selenaklasnja.myapplication.presentation.methodselection

import com.example.selenaklasnja.myapplication.domain.data.MethodSelection
import com.example.selenaklasnja.myapplication.domain.repositories.AppDataRepository
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations


class MethodSelectionPresenterTest {
    @Mock
    lateinit var view: MethodSelectionPresenter.ViewSurface

    @Mock
    lateinit var appDataRepository: AppDataRepository

    lateinit var presenter: MethodSelectionPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = MethodSelectionPresenter(appDataRepository)
        presenter.onCreate(view)
    }

    @Test
    fun `onBuySelected - buy button is selected - selection is saved`() {
        // when
        presenter.onBuySelected()

        // then
        verify(appDataRepository, times(1)).saveMethodSelection(MethodSelection.BUY)
    }

    @Test
    fun `onBuySelected - buy button is selected - select currency activity will be started`() {
        // when
        presenter.onBuySelected()

        // then
        verify(view, times(1)).startSelectActivity()
    }

    @Test
    fun `onSellSelected - sell button is selected - selection is saved`() {
        // when
        presenter.onSellSelected()

        // then
        verify(appDataRepository, times(1)).saveMethodSelection(MethodSelection.SELL)
    }


    @Test
    fun `onSellSelected - sell button is selected - select currency activity will be started`() {
        // when
        presenter.onSellSelected()

        // then
        verify(view, times(1)).startSelectActivity()
    }
}