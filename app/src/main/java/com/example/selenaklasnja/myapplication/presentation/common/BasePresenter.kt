package com.example.selenaklasnja.myapplication.presentation.common

import com.example.selenaklasnja.myapplication.Schedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


open class BasePresenter {
    protected val tasksWhileInMemory = CompositeDisposable()

    @Inject
    lateinit var schedulers: Schedulers

    open fun onViewPaused() {

    }

    open fun onViewStarted() {

    }

    open fun onViewStopped() {

    }

    open fun onViewDestroyed() {
        tasksWhileInMemory.clear()
    }
}