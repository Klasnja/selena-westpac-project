package com.example.selenaklasnja.myapplication.injection.modules

import android.app.Activity
import android.content.Context
import android.support.v7.app.AppCompatActivity
import com.example.selenaklasnja.myapplication.injection.annotations.ActivityContext
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: AppCompatActivity) {

    @Provides
    fun provideAppCompatActivity(): AppCompatActivity = activity

    @Provides
    fun provideActivity(): Activity = activity

    @Provides
    @ActivityContext
    fun provideContext(): Context = activity

}
