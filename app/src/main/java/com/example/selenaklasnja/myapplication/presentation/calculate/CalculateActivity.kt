package com.example.selenaklasnja.myapplication.presentation.calculate

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.selenaklasnja.myapplication.R
import com.example.selenaklasnja.myapplication.data.Currency
import com.example.selenaklasnja.myapplication.domain.data.MethodSelection
import com.example.selenaklasnja.myapplication.injection.components.DaggerActivityComponent
import com.example.selenaklasnja.myapplication.injection.modules.ActivityModule
import com.example.selenaklasnja.myapplication.presentation.common.BaseActivity
import com.jakewharton.rxbinding2.widget.RxTextView
import kotlinx.android.synthetic.main.activity_calculate.*
import java.text.NumberFormat
import java.util.*


class CalculateActivity : BaseActivity<CalculatePresenter>(), CalculatePresenter.ViewSurface {

    companion object {
        val LOCALE = Locale("en", "AU")
        const val METHOD = "METHOD"
        const val CURRENCY_NAME = "CURRENCY_NAME"
        const val EXCHANGE_RATE = "EXCHANGE_RATE"
        const val EMPTY_STRING = ""

        fun intent(context: Context, method: Int, currency: Currency, rate: Number): Intent {
            val intent = Intent(context, CalculateActivity::class.java)
            intent.putExtra(METHOD, method)
            intent.putExtra(CURRENCY_NAME, currency)
            intent.putExtra(EXCHANGE_RATE, rate)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculate)
        presenter.onCreate(this)
    }

    override fun onResume() {
        super.onResume()
        val currency = intent.getParcelableExtra<Currency>(CURRENCY_NAME)
        val rate = intent.getDoubleExtra(EXCHANGE_RATE, 1.0)
        val method = MethodSelection.values()[intent.getIntExtra(METHOD, 0)]
        val methodName = getString(method.methodRes)

        val inputLabelText = getString(R.string.input_amount_label, currency.country, currency.name, methodName)
        input_title.text = inputLabelText

        with(tasksWhileActive) {
            add(RxTextView.textChanges(edt_input_amount)
                    .subscribe {
                        val inputText = edt_input_amount.text.toString()
                        txt_output_amount.text = getConvertedValue(inputText, rate)
                    })
        }
    }

    override fun initActivityComponent() {
        DaggerActivityComponent.builder()
                .applicationComponent(getAppComponent())
                .activityModule(ActivityModule(this))
                .build()
                .inject(this)
    }

    override fun shouldShowBackButton() = true

    private fun getConvertedValue(value: String, rate: Double) =
            if (value.isNotBlank()) {
                try {
                    (value.toDouble() / rate).localisedString()
                } catch (e: Exception) {
                    EMPTY_STRING
                }

            } else {
                EMPTY_STRING
            }


    private fun Double.localisedString(): String = NumberFormat.getNumberInstance(LOCALE).format(this)

}
