package com.example.selenaklasnja.myapplication

import android.app.Application
import com.example.selenaklasnja.myapplication.injection.components.ApplicationComponent
import com.example.selenaklasnja.myapplication.injection.components.DaggerApplicationComponent
import com.example.selenaklasnja.myapplication.injection.modules.ApplicationModule
import javax.inject.Inject


class Application : Application() {

    @Inject
    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        initApplicationComponent()
    }

    private fun initApplicationComponent() {
        DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build().inject(this)
    }
}