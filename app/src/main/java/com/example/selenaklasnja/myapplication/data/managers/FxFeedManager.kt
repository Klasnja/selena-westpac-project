package com.example.selenaklasnja.myapplication.data.managers

import com.example.fxservice.service.FxConverter
import com.example.fxservice.service.FxFeed
import com.example.selenaklasnja.myapplication.Schedulers
import com.example.selenaklasnja.myapplication.data.Currency
import com.example.selenaklasnja.myapplication.data.CurrencyMapper
import com.example.selenaklasnja.myapplication.domain.repositories.FxFeedRepository
import io.reactivex.Notification
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject


class FxFeedManager @Inject constructor(private val schedulers: Schedulers) : FxFeedRepository {

    @Volatile
    private var dataFeeds: BehaviorSubject<Notification<Map<String, FxFeed>>> = BehaviorSubject.create()

    init {
        downloadFxMap()
    }

    override fun downloadFxMap() {
        FxConverter().getFxMap()
                .observeOn(schedulers.ui())
                .subscribe({
                    dataFeeds.onNext(Notification.createOnNext(it))
                }, { throwable ->
                    val exception = when(throwable) {
                        is ConnectException -> NoNetworkException()
                        is SocketException -> NoNetworkException()
                        is UnknownHostException -> NoNetworkException()
                        is SocketTimeoutException -> NetworkTimeoutException()
                        else-> throwable
                    }
                    dataFeeds.onNext(Notification.createOnError(exception))
                })
    }

    override fun getCurrencyList(): Observable<Notification<Collection<Currency>>> {
        return dataFeeds
                .map { notification ->
                    if(notification.isOnError) {
                        Notification.createOnError(notification.error!!)
                    } else {
                        Notification.createOnNext(CurrencyMapper.mapCurrencySet(notification.value!!.values))
                    }
                }
    }

    override fun getClientBuyExchangeRate(currency: String): Observable<Number> {
        return dataFeeds
                .map { notification ->
                    if(notification.isOnError) {
                        throw notification.error!!
                    } else {
                        val currencyDetails = notification.value!![currency] ?: throw IllegalArgumentException("Nu such currency $currency")
                        currencyDetails.sellRate
                    }
                }
    }

    override fun getClientSellExchangeRate(currency: String): Observable<Number> {
        return dataFeeds
                .map { notification ->
                    if(notification.isOnError) {
                        throw notification.error!!
                    } else {
                        val currencyDetails = notification.value!![currency] ?: throw IllegalArgumentException("Nu such currency $currency")
                        currencyDetails.buyRate
                    }
                }
    }
}

class NoNetworkException : Exception()
class NetworkTimeoutException : Exception()