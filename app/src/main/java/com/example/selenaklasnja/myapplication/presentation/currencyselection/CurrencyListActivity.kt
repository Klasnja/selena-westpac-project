package com.example.selenaklasnja.myapplication.presentation.currencyselection

import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.widget.LinearLayout
import com.example.selenaklasnja.myapplication.R
import com.example.selenaklasnja.myapplication.data.Currency
import com.example.selenaklasnja.myapplication.injection.components.DaggerActivityComponent
import com.example.selenaklasnja.myapplication.injection.modules.ActivityModule
import com.example.selenaklasnja.myapplication.presentation.calculate.CalculateActivity
import com.example.selenaklasnja.myapplication.presentation.common.BaseActivity
import kotlinx.android.synthetic.main.activity_currency_list.*

class CurrencyListActivity : BaseActivity<CurrencyListPresenter>(), CurrencyListPresenter.ViewSurface {

    private lateinit var adapter: CurrencyListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currency_list)
        presenter.onCreate(this)

        setupRecyclerView()
    }

    override fun onResume() {
        super.onResume()

        with(tasksWhileActive) {
            add(eventBus.listenTo(CurrencySelectedEvent::class.java)
                    .observeOn(schedulers.ui())
                    .subscribe { presenter.onCurrencySelected(it.currency) })
        }

        presenter.getCurrencyList()
    }

    override fun initActivityComponent() {
        DaggerActivityComponent.builder()
                .applicationComponent(getAppComponent())
                .activityModule(ActivityModule(this))
                .build()
                .inject(this)
    }

    private fun setupRecyclerView() {
        adapter = CurrencyListAdapter(eventBus)
        val layoutManager = LinearLayoutManager(this)
        recycle_view_currency_list.layoutManager = layoutManager
        recycle_view_currency_list.setHasFixedSize(true)

        val dividerItemDecoration = DividerItemDecoration(this, LinearLayout.VERTICAL)
        ContextCompat.getDrawable(this, R.drawable.divider_view)?.let {
            dividerItemDecoration.setDrawable(it)
        }

        recycle_view_currency_list.addItemDecoration(dividerItemDecoration)

        recycle_view_currency_list.adapter = adapter
    }

    override fun updateList(viewList: List<CurrencyListAdapter.CurrencyViewItem>) {
        adapter.updateData(viewList)
    }

    override fun showError(@StringRes messageId: Int) {
        showErrorDialog(getString(messageId), { presenter.retryClicked() })
    }

    override fun startCalculateActivity(method: Int, currency: Currency, rate: Number) {
        startActivity(CalculateActivity.intent(this, method, currency, rate))
    }

    override fun shouldShowBackButton() = true
}
