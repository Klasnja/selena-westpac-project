package com.example.selenaklasnja.myapplication.presentation.currencyselection

import com.example.selenaklasnja.myapplication.data.Currency
import com.example.selenaklasnja.myapplication.presentation.currencyselection.CurrencyListAdapter.CurrencyListViewType
import com.example.selenaklasnja.myapplication.presentation.currencyselection.CurrencyListAdapter.CurrencyViewItem
import javax.inject.Inject

class ListItemsCreator @Inject constructor() {

    fun createProgressBar(): List<CurrencyViewItem> {
        val list: MutableList<CurrencyViewItem> = mutableListOf()
        list.add(CurrencyViewItem(CurrencyListViewType.PROGRESS_BAR))
        return list
    }

    fun createUiModel(currencyList: Collection<Currency>): List<CurrencyViewItem> {
        val list: MutableList<CurrencyViewItem> = mutableListOf()
        currencyList.map {
            list.add(CurrencyViewItem(CurrencyListViewType.ITEM, it))
        }
        list.add(CurrencyViewItem(CurrencyListViewType.FOOTER))
        return list
    }
}