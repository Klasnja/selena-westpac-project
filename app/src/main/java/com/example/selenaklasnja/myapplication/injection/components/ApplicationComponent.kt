package com.example.selenaklasnja.myapplication.injection.components

import com.example.selenaklasnja.myapplication.Application
import com.example.selenaklasnja.myapplication.injection.modules.ApplicationModule
import com.example.selenaklasnja.myapplication.injection.modules.SingletonModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class, SingletonModule::class))
interface ApplicationComponent : Singletons {

    fun inject(application: Application)
}