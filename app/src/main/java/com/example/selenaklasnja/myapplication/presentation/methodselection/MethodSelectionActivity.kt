package com.example.selenaklasnja.myapplication.presentation.methodselection

import android.content.Intent
import android.os.Bundle
import com.example.selenaklasnja.myapplication.R
import com.example.selenaklasnja.myapplication.injection.components.DaggerActivityComponent
import com.example.selenaklasnja.myapplication.injection.modules.ActivityModule
import com.example.selenaklasnja.myapplication.presentation.common.BaseActivity
import com.example.selenaklasnja.myapplication.presentation.currencyselection.CurrencyListActivity
import com.jakewharton.rxbinding2.view.RxView
import kotlinx.android.synthetic.main.activity_method_selection.*

class MethodSelectionActivity : BaseActivity<MethodSelectionPresenter>(), MethodSelectionPresenter.ViewSurface {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_method_selection)
        presenter.onCreate(this)
    }

    override fun onResume() {
        super.onResume()
        with(tasksWhileActive) {
            add(RxView.clicks(layout_buy)
                    .subscribe {
                       presenter.onBuySelected()
                    })
            add(RxView.clicks(layout_sell)
                    .subscribe {
                        presenter.onSellSelected()
                    })
        }
    }

    override fun initActivityComponent() {
        DaggerActivityComponent.builder()
                .applicationComponent(getAppComponent())
                .activityModule(ActivityModule(this))
                .build()
                .inject(this)
    }


    override fun startSelectActivity() {
        startActivity(Intent(this, CurrencyListActivity::class.java))
    }

    override fun shouldShowBackButton() = false
}
