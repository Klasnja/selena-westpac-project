package com.example.selenaklasnja.myapplication.domain.repositories

import com.example.selenaklasnja.myapplication.data.Currency
import io.reactivex.Notification
import io.reactivex.Observable


interface FxFeedRepository {
    /**
     * Downloads fx map
     *
     */
    fun downloadFxMap()

    /**
     * Returns collection of currencies
     *
     * @return Observable that returns set of known currencies
     */
    fun getCurrencyList(): Observable<Notification<Collection<Currency>>>


    /**
     * Returns exchange rate when buying the currency
     *
     * @param currency name of currency
     *
     * @return Observable that returns current buying exchange rate of the currency
     */
    fun getClientBuyExchangeRate(currency: String): Observable<Number>


    /**
     * Returns exchange rate when selling the currency
     *
     * @param currency name of currency
     *
     * @return Observable that returns current selling exchange rate of the currency
     */
    fun getClientSellExchangeRate(currency: String): Observable<Number>
}