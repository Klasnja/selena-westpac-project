package com.example.selenaklasnja.myapplication.data

import com.example.fxservice.service.FxFeed



class CurrencyMapper {

    companion object {
        /**
         * Converts collection of FxFeed to collection of currency
         *
         * @param currencies collection of FxFeed
         *
         * @return collection of currency
         */
        fun mapCurrencySet(currencies: Collection<FxFeed>) : Collection<Currency> =
                currencies.map { Currency(it.currencyCode, it.currencyName, it.country) }

    }
}