package com.example.selenaklasnja.myapplication.injection.modules

import com.example.selenaklasnja.myapplication.data.managers.AppDataManager
import com.example.selenaklasnja.myapplication.data.managers.FxFeedManager
import com.example.selenaklasnja.myapplication.domain.repositories.AppDataRepository
import com.example.selenaklasnja.myapplication.domain.repositories.FxFeedRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface SingletonModule {
    @Binds
    @Singleton
    fun bindFxFeedRepository(fxFeedManager: FxFeedManager): FxFeedRepository

    @Binds
    @Singleton
    fun bindAppDataRepository(appDataManager: AppDataManager): AppDataRepository
}