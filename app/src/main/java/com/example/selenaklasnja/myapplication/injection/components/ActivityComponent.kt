package com.example.selenaklasnja.myapplication.injection.components

import android.content.Context
import android.support.v7.app.AppCompatActivity
import com.example.selenaklasnja.myapplication.injection.annotations.ActivityContext
import com.example.selenaklasnja.myapplication.injection.annotations.PerActivity
import com.example.selenaklasnja.myapplication.injection.modules.ActivityModule
import com.example.selenaklasnja.myapplication.presentation.currencyselection.CurrencyListActivity
import com.example.selenaklasnja.myapplication.presentation.calculate.CalculateActivity
import com.example.selenaklasnja.myapplication.presentation.methodselection.MethodSelectionActivity
import dagger.Component

@PerActivity
@Component(dependencies = arrayOf(ApplicationComponent::class),
        modules = arrayOf(ActivityModule::class))
interface ActivityComponent : Singletons {

    fun provideAppCompatActivity(): AppCompatActivity

    @ActivityContext
    fun provideActivityContext(): Context

    fun inject(activity: CalculateActivity)
    fun inject(activity: MethodSelectionActivity)
    fun inject(activity: CurrencyListActivity)

}