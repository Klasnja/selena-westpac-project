package com.example.selenaklasnja.myapplication.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Currency(val code: String, val name: String, val country: String) : Parcelable