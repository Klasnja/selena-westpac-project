package com.example.selenaklasnja.myapplication.presentation.currencyselection

import android.support.annotation.StringRes
import com.example.selenaklasnja.myapplication.R
import com.example.selenaklasnja.myapplication.data.Currency
import com.example.selenaklasnja.myapplication.data.managers.NetworkTimeoutException
import com.example.selenaklasnja.myapplication.data.managers.NoNetworkException
import com.example.selenaklasnja.myapplication.domain.data.MethodSelection
import com.example.selenaklasnja.myapplication.domain.repositories.AppDataRepository
import com.example.selenaklasnja.myapplication.domain.repositories.FxFeedRepository
import com.example.selenaklasnja.myapplication.presentation.common.BasePresenter
import javax.inject.Inject


class CurrencyListPresenter @Inject constructor(private val fxFeedRepository: FxFeedRepository,
                                                private val appDataRepository: AppDataRepository,
                                                private val listItemsCreator: ListItemsCreator) : BasePresenter() {
    private lateinit var view: ViewSurface

    fun onCreate(view: ViewSurface) {
        this.view = view
    }

    fun getCurrencyList() {
        view.updateList(listItemsCreator.createProgressBar())
        fxFeedRepository.getCurrencyList()
                .observeOn(schedulers.ui())
                .subscribe({ notification ->
                    if (notification.isOnError) {
                        view.updateList(emptyList<CurrencyListAdapter.CurrencyViewItem>())
                        val throwable = notification.error
                        when (throwable) {
                            is NoNetworkException, is NetworkTimeoutException -> view.showError(R.string.error_network_connection)
                            else -> view.showError(R.string.error_server)
                        }
                    } else {
                        view.updateList(listItemsCreator.createUiModel(notification.value!!))
                    }
                })
    }

    fun onCurrencySelected(currency: Currency) {
        val methodSelection = appDataRepository.getMethodSelection()
        getExchangeMethod(currency.code, methodSelection)
                .firstOrError()
                .observeOn(schedulers.ui())
                .subscribe({
                    view.startCalculateActivity(methodSelection.ordinal, currency, it)
                }, { throwable ->
                    when (throwable) {
                        is NoNetworkException, is NetworkTimeoutException -> view.showError(R.string.error_network_connection)
                        else -> view.showError(R.string.error_server)

                    }
                })
    }

    fun retryClicked() {
        fxFeedRepository.downloadFxMap()
    }

    private fun getExchangeMethod(name: String, methodSelection: MethodSelection) = when (methodSelection) {
        MethodSelection.BUY -> fxFeedRepository.getClientBuyExchangeRate(name)
        else -> fxFeedRepository.getClientSellExchangeRate(name)
    }


    interface ViewSurface {
        fun updateList(viewList: List<CurrencyListAdapter.CurrencyViewItem>)
        fun showError(@StringRes messageId: Int)
        fun startCalculateActivity(method: Int, currency: Currency, rate: Number)
    }
}