package com.example.selenaklasnja.myapplication.domain.data

import android.support.annotation.StringRes
import com.example.selenaklasnja.myapplication.R

enum class MethodSelection (@StringRes val methodRes: Int){
    BUY(R.string.buy),
    SELL(R.string.sell)
}