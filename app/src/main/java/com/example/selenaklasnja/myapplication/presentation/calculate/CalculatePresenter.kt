package com.example.selenaklasnja.myapplication.presentation.calculate

import com.example.selenaklasnja.myapplication.presentation.common.BasePresenter
import javax.inject.Inject


class CalculatePresenter @Inject constructor(): BasePresenter() {
    private lateinit var view: ViewSurface

    fun onCreate(view: ViewSurface){
        this.view = view
    }

    interface ViewSurface {
    }
}