package com.example.selenaklasnja.myapplication.injection.components

import android.content.Context
import com.example.selenaklasnja.myapplication.Schedulers
import com.example.selenaklasnja.myapplication.domain.repositories.AppDataRepository
import com.example.selenaklasnja.myapplication.domain.repositories.FxFeedRepository
import com.example.selenaklasnja.myapplication.injection.annotations.AppContext
import com.example.selenaklasnja.myapplication.utils.RxEventBus


interface Singletons {

    @AppContext
    fun provideContext(): Context

    fun provideSchedulers(): Schedulers

    fun provideFxFeedRepository(): FxFeedRepository

    fun provideAppDataRepository(): AppDataRepository

    fun provideRxEventBus(): RxEventBus
}