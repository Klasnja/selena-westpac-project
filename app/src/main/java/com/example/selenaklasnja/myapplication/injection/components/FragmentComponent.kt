package com.example.selenaklasnja.myapplication.injection.components

import com.example.selenaklasnja.myapplication.injection.modules.FragmentModule
import com.example.selenaklasnja.myapplication.injection.annotations.PerFragment
import dagger.Component

@PerFragment
@Component(dependencies = arrayOf(ActivityComponent::class),
        modules = arrayOf(FragmentModule::class))
interface FragmentComponent {

}