package com.example.selenaklasnja.myapplication.presentation.currencyselection

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.selenaklasnja.myapplication.R
import com.example.selenaklasnja.myapplication.data.Currency
import com.example.selenaklasnja.myapplication.presentation.currencyselection.CurrencyListAdapter.CurrencyListViewType.*
import com.example.selenaklasnja.myapplication.utils.RxEventBus
import kotlinx.android.synthetic.main.list_item_currency_detail_item.view.*


class CurrencyListAdapter(val eventBus: RxEventBus) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    data class CurrencyViewItem(val viewType: CurrencyListViewType, val data: Any? = null)

    enum class CurrencyListViewType {
        PROGRESS_BAR,
        ITEM,
        FOOTER
    }

    private val currencyListViewItemList = emptyList<CurrencyViewItem>().toMutableList()

    fun updateData(viewList: List<CurrencyViewItem>) {
        currencyListViewItemList.clear()
        currencyListViewItemList.addAll(viewList)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is CurrencyListItemViewHolder -> holder.bindData(eventBus, currencyListViewItemList[position].data as Currency)
        }
    }

    override fun getItemCount(): Int = currencyListViewItemList.size

    override fun getItemViewType(position: Int) = currencyListViewItemList[position].viewType.ordinal


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = when (viewType) {
        FOOTER.ordinal -> {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_item_select_currency_footer, parent, false)
            CurrencyListHeaderViewHolder(view)
        }
        PROGRESS_BAR.ordinal -> {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_item_circle_progress_bar, parent, false)
            ProgressBarViewHolder(view)
        }
        ITEM.ordinal -> {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_item_currency_detail_item, parent, false)
            CurrencyListItemViewHolder(view)
        }
        else -> throw RuntimeException("not view type matched")
    }

    class CurrencyListHeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    class ProgressBarViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    class CurrencyListItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(eventBus: RxEventBus, currency: Currency) {
            itemView.txt_currency_code.text = currency.code
            itemView.txt_currency_name.text = currency.name
            itemView.txt_currency_country.text = currency.country
            itemView.layout_currency_item.setOnClickListener { eventBus.publish(CurrencySelectedEvent(currency)) }
        }
    }
}

data class CurrencySelectedEvent(val currency: Currency)