package com.example.selenaklasnja.myapplication.domain.repositories

import com.example.selenaklasnja.myapplication.domain.data.MethodSelection


interface AppDataRepository {
    /**
     * Saves method selection
     *
     */
    fun saveMethodSelection(method: MethodSelection)

    /**
     * Gets method selection
     *
     */
    fun getMethodSelection() : MethodSelection
}