package com.example.selenaklasnja.myapplication.presentation.methodselection

import com.example.selenaklasnja.myapplication.domain.data.MethodSelection
import com.example.selenaklasnja.myapplication.domain.repositories.AppDataRepository
import com.example.selenaklasnja.myapplication.presentation.common.BasePresenter
import javax.inject.Inject


class MethodSelectionPresenter @Inject constructor(private val appDataRepository: AppDataRepository): BasePresenter() {
    private lateinit var view: ViewSurface

    fun onCreate(view: ViewSurface){
        this.view = view
    }

    fun onBuySelected() {
        appDataRepository.saveMethodSelection(MethodSelection.BUY)
        view.startSelectActivity()
    }

    fun onSellSelected() {
        appDataRepository.saveMethodSelection(MethodSelection.SELL)
        view.startSelectActivity()
    }

    interface ViewSurface {
        fun startSelectActivity()
    }
}