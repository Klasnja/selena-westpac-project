package com.example.selenaklasnja.myapplication.presentation.common

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.example.selenaklasnja.myapplication.Application
import com.example.selenaklasnja.myapplication.R
import com.example.selenaklasnja.myapplication.Schedulers
import com.example.selenaklasnja.myapplication.injection.components.ApplicationComponent
import com.example.selenaklasnja.myapplication.utils.RxEventBus
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


abstract class BaseActivity<PresenterType : BasePresenter> : AppCompatActivity() {

    @Inject
    lateinit var presenter: PresenterType

    @Inject
    lateinit var eventBus: RxEventBus

    @Inject
    lateinit var schedulers: Schedulers

    protected val tasksWhileActive = CompositeDisposable()

    private var dialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initActivityComponent()
        if (shouldShowBackButton()) showBackButton()
    }

    abstract fun initActivityComponent()

    override fun onPause() {
        super.onPause()
        presenter.onViewPaused()
        tasksWhileActive.clear()
    }

    override fun onStart() {
        super.onStart()
        presenter.onViewStarted()
    }

    override fun onStop() {
        presenter.onViewStopped()
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        dialog?.dismiss()
        presenter.onViewDestroyed()
    }

    private fun showBackButton() {
        supportActionBar?.apply {
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            android.R.id.home -> finish()
        }

        return super.onOptionsItemSelected(menuItem)
    }

    fun getAppComponent(): ApplicationComponent = (application as Application).applicationComponent

    protected abstract fun shouldShowBackButton(): Boolean

    fun showErrorDialog(message: String, retryMethod: () -> Unit) {
        val builder = AlertDialog.Builder(this, R.style.AlertDialog)
                .setCancelable(true)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.error)
                .setMessage(message)

        builder.setNeutralButton(getString(R.string.retry)) { _, _ -> retryMethod() }

        dialog = builder.create()
        dialog!!.show()
    }
}