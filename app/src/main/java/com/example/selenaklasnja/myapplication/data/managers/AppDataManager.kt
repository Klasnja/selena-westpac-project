package com.example.selenaklasnja.myapplication.data.managers

import com.example.selenaklasnja.myapplication.domain.data.MethodSelection
import com.example.selenaklasnja.myapplication.domain.repositories.AppDataRepository
import javax.inject.Inject


class AppDataManager @Inject constructor() : AppDataRepository {

    private lateinit var selectedMethod: MethodSelection
    private lateinit var selectedCurrency: String

    @Synchronized
    override fun saveMethodSelection(method: MethodSelection) {
        selectedMethod = method
    }
    @Synchronized
    override fun getMethodSelection()= selectedMethod
}