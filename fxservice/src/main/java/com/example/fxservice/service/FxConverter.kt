package com.example.fxservice.service

import com.example.selenaklasnja.fxfeedlibrary.data.api.MyAppApi
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


/**
 *
 * <code>FxConverter</code> is a service class that will provide with
 * fx feeds data retrieved from a server with a given url
 *
 * @param url                   base api url for fetching the fx feeds
 * @param apiConnectTimeout     the default connect timeout for the connections
 * @param apiReadTimeout        the default read timeout for the connections
 * @param apiWriteTimeout       the default write timeout for the connections
 *
 */

class FxConverter constructor(url: String = URL,
                              apiConnectTimeout: Long = API_CONNECT_TIMEOUT,
                              apiReadTimeout: Long = API_READ_TIMEOUT,
                              apiWriteTimeout: Long = API_WRITE_TIMEOUT) {

    companion object {
        val URL = "https://www.westpac.com.au/bin/"
        val API_CONNECT_TIMEOUT: Long = 60
        val API_READ_TIMEOUT: Long = 60
        val API_WRITE_TIMEOUT: Long = 60
    }

    private val fxApi: MyAppApi

    init {
        val okHttpClient = OkHttpClient().newBuilder()
                .connectTimeout(apiConnectTimeout, TimeUnit.SECONDS)
                .readTimeout(apiReadTimeout, TimeUnit.SECONDS)
                .writeTimeout(apiWriteTimeout, TimeUnit.SECONDS)
                .build()

        val builder = Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))

        fxApi = buildAppApi(builder, MyAppApi::class.java)
    }

    /**
     * Returns all fx data
     *
     * @return Single that returns all the fx Data
     */
    fun getFxData() = fxApi.getExchangeRate()


    /**
     * Maps currency to buy and sell fx rates
     *
     * @return Single that returns map of currency fx buying and selling rates
     *
     * Rates are given as Double values
     */
    fun getFxMap(): Single<Map<String, FxFeed>> {
        return getFxData().map { fxData ->
            val fxMap = mutableMapOf<String, FxFeed>()
            fxData.data.brands.wbc.portfolios.fx.products.forEach { (name, detail) ->
                val feed = detail.rates[name]
                val buyRate = feed?.buyTT?.toDoubleOrNull()
                val sellRate = feed?.sellTT?.toDoubleOrNull()
                if (sellRate != null && buyRate != null) {
                    fxMap[name] = FxFeed(feed.currencyCode, feed.currencyName, feed.country, buyRate, sellRate)
                }
            }
            fxMap
        }
    }


    private fun <T> buildAppApi(retrofitBuilder: Retrofit.Builder, service: Class<out T>): T =
            retrofitBuilder
                    .build()
                    .create(service)

}

data class FxFeed(val currencyCode: String,
                  val currencyName: String,
                  val country: String,
                  val buyRate: Number,
                  val sellRate: Number)