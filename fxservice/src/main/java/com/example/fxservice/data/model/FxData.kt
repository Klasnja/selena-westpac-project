package com.example.fxservice.data.model

import com.google.gson.annotations.SerializedName

data class FxData(@SerializedName("data") val data: Data)

data class Data(@SerializedName("Brands") val brands: Brands)

data class Brands(@SerializedName("WBC") val wbc: Wbc)

data class Wbc(@SerializedName("Portfolios") val portfolios: Portfolios)

data class Portfolios(@SerializedName("FX") val fx: FxProducts)

data class FxProducts(
        @SerializedName("Products") val products: Map<String, ProductDetail>)

data class ProductDetail(
        @SerializedName("ProductId") val productId: String,
        @SerializedName("Rates") val rates: Map<String, FxFeedInStrings>)

data class FxFeedInStrings(
        @SerializedName("currencyCode") val currencyCode: String,
        @SerializedName("currencyName") val currencyName: String,
        @SerializedName("country") val country: String,
        @SerializedName("buyTT") val buyTT: String,
        @SerializedName("sellTT") val sellTT: String)

