package com.example.selenaklasnja.fxfeedlibrary.data.api

import com.example.fxservice.data.model.FxData
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers


interface MyAppApi {
    @Headers("Content-Type:application/json")
    @GET("getJsonRates.wbc.fx.json")
    fun getExchangeRate() : Single<FxData>
}