@file:Suppress("IllegalIdentifier")

package com.example.fxservice.service

import com.example.fxservice.data.model.*
import com.google.gson.GsonBuilder
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.HttpException
import java.util.concurrent.TimeUnit


class FxConverterTest {

    //region  Default Values
    val path = "/getJsonRates.wbc.fx.json"

    private val buyTTUsd = "0.7877"
    private val sellTTUsd = "0.7148"

    private val buyTTEur = "0.6783"
    private val sellTTEur = "0.6121"

    private val buyTTCny = "N/A"
    private val sellTTCny = "On App"
    //endregion

    private lateinit var server: MockWebServer
    private lateinit var fxConverter: FxConverter

    @Before
    fun setUp() {

        server = MockWebServer()
        fxConverter = FxConverter(url = server.url("/").toString())
    }

    @After
    fun tearDown() {
        server.shutdown()
    }


    @Test
    fun `getFxData - api is successful - returns correct data`() {
        /// given
        val expectedResponse = createSampleFxDataResponse()
        server.enqueue(createMockResponse(200, toJson(expectedResponse)))

        // when
        val testObserver = fxConverter.getFxData().test()
        testObserver.awaitTerminalEvent(2, TimeUnit.SECONDS)

        // then
        testObserver.assertSubscribed()
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertResult(expectedResponse)
        assertThat(server.takeRequest().path, `is`(path))
    }

    @Test
    fun `getFxData - api returns a http exception - throws the exception`() {
        /// given
        val expectedCode = 500
        server.enqueue(createMockResponse(expectedCode, toJson("Oops")))

        // when
        val testObserver = fxConverter.getFxData().test()
        testObserver.awaitTerminalEvent(2, TimeUnit.SECONDS)

        // then
        testObserver.assertSubscribed()
        val errors = testObserver.errors()
        assertThat(errors.count(), `is`(1))
        val error = errors[0]
        assertThat(error is HttpException, `is`(true))
        val httpError = error as HttpException
        assertThat(httpError.code(), `is`(expectedCode))
    }

    @Test
    fun `getFxMap - api is successful - returns correct data`() {
        /// given
        val expectedResponse = createFxMapResponse()
        server.enqueue(createMockResponse(200, toJson(createSampleFxDataResponse())))

        // when
        val testObserver = fxConverter.getFxMap().test()
        testObserver.awaitTerminalEvent(2, TimeUnit.SECONDS)

        // then
        testObserver.assertSubscribed()
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertResult(expectedResponse)
        assertThat(server.takeRequest().path, `is`(path))
    }

    private fun createFxMapResponse() = mapOf(
            "USD" to FxFeed("USD", "Dollars", "USA", buyTTUsd.toDouble(), sellTTUsd.toDouble()),
            "EUR" to FxFeed("EUR", "Euro", "Europe", buyTTEur.toDouble(), sellTTEur.toDouble()))


    private fun createSampleFxDataResponse(): FxData {
        val mapProducts = mapOf(
                "USD" to
                        ProductDetail("USD", mapOf("USD" to
                                FxFeedInStrings("USD", "Dollars", "USA", buyTTUsd, sellTTUsd))),
                "EUR" to
                        ProductDetail("EUR", mapOf("EUR" to
                                FxFeedInStrings("EUR", "Euro", "Europe", buyTTEur, sellTTEur))),
                "CNY" to
                        ProductDetail("CNY", mapOf("CNY" to
                                FxFeedInStrings("CNY", "Renminbi Yuan", "China", buyTTCny, sellTTCny)))
        )

        return FxData(Data(Brands(Wbc(Portfolios(FxProducts(mapProducts))))))
    }

    private fun createMockResponse(responseCode: Int, responseBody: String) =
            MockResponse()
                    .setBody(responseBody)
                    .setResponseCode(responseCode)

    private fun toJson(o: Any): String = GsonBuilder().create().toJson(o)
}